if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/removeFSLWrapper design_ttest2 fdr randomise randomise_combine randomise_parallel setup_masks tfce_support unconfound
fi
